import React from 'react';

const BgFiller = () => (
  <div
    style={{
      width: '100vw',
      background: '#eee',
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      zIndex: -1,
    }}
  >
    <div
      style={{
        width: '50%',
        background: 'var(--upd-navy)',
        position: 'absolute',
        top: 0,
        bottom: 0,
      }}
    >
      &nbsp;
    </div>
  </div>
);

export default BgFiller;
