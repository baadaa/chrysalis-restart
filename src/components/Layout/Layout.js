import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Header from '../Header';
import Sidebar from '../Sidebar/Sidebar';
import BgFiller from './BackgroundFiller';
import './gatsby-normalize.css';
import './Layout.scss';

const ContentArea = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  min-height: calc(100vh - var(--header-height));
  display: flex;
  background: #fff;
  box-sizing: border-box;
  border-right: 1px solid #ccc;
  position: relative;
  main {
    flex: 1;
    position: relative;
    padding: 3rem 4rem;
    padding-bottom: 5rem;
    transition: transform .2s;
    @media screen and (max-width: 910px) {
      transform: translateX(var(--sidebar-width)));
      padding: 2.3rem;
    }
  }
`;

const Footer = styled.footer`
  text-align: center;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 0.5rem;
  font-size: 1.2rem;
  background: var(--upd-pink);
  color: #fff;
  font-family: var(--sans);
  z-index: 99;
`;

const Layout = ({ children }) => (
  <>
    <Header />
    <ContentArea>
      <Sidebar />
      <main>{children}</main>
    </ContentArea>
    <BgFiller />
    <Footer>&copy;{new Date().getFullYear()} Updater</Footer>
  </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
