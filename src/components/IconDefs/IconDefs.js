/* eslint-disable camelcase */

// App Features
import accountUpdateOutline from '../../images/icons/app_features/icon-app-feature-account-update-circle-outline.svg';
import accountUpdateNoCircle from '../../images/icons/app_features/icon-app-feature-account-update.svg';
import accountUpdateCircle from '../../images/icons/app_features/icon-app-feature-account-update-circle.svg';
import diyMovingOutline from '../../images/icons/app_features/icon-app-feature-diy-moving-circle-outline.svg';
import diyMovingNoCircle from '../../images/icons/app_features/icon-app-feature-diy-moving.svg';
import diyMovingCircle from '../../images/icons/app_features/icon-app-feature-diy-moving-circle.svg';
import driverLicenseOutline from '../../images/icons/app_features/icon-app-feature-driver-license-circle-outline.svg';
import driverLicenseNoCircle from '../../images/icons/app_features/icon-app-feature-driver-license.svg';
import driverLicenseCircle from '../../images/icons/app_features/icon-app-feature-driver-license-circle.svg';
import homeSecurityOutline from '../../images/icons/app_features/icon-app-feature-home-security-circle-outline.svg';
import homeSecurityNoCircle from '../../images/icons/app_features/icon-app-feature-home-security.svg';
import homeSecurityCircle from '../../images/icons/app_features/icon-app-feature-home-security-circle.svg';
import insuranceOutline from '../../images/icons/app_features/icon-app-feature-insurance-circle-outline.svg';
import insuranceNoCircle from '../../images/icons/app_features/icon-app-feature-insurance.svg';
import insuranceCircle from '../../images/icons/app_features/icon-app-feature-insurance-circle.svg';
import exclusiveOffersOutline from '../../images/icons/app_features/icon-app-feature-exclusive-offers-circle-outline.svg';
import exclusiveOffersNoCircle from '../../images/icons/app_features/icon-app-feature-exclusive-offers.svg';
import exclusiveOffersCircle from '../../images/icons/app_features/icon-app-feature-exclusive-offers-circle.svg';
import localGuideOutline from '../../images/icons/app_features/icon-app-feature-local-guide-circle-outline.svg';
import localGuideNoCircle from '../../images/icons/app_features/icon-app-feature-local-guide.svg';
import localGuideCircle from '../../images/icons/app_features/icon-app-feature-local-guide-circle.svg';
import localServicesOutline from '../../images/icons/app_features/icon-app-feature-local-services-circle-outline.svg';
import localServicesNoCircle from '../../images/icons/app_features/icon-app-feature-local-services.svg';
import localServicesCircle from '../../images/icons/app_features/icon-app-feature-local-services-circle.svg';
import mailforwardingOutline from '../../images/icons/app_features/icon-app-feature-mailforwarding-circle-outline.svg';
import mailforwardingNoCircle from '../../images/icons/app_features/icon-app-feature-mailforwarding.svg';
import mailforwardingCircle from '../../images/icons/app_features/icon-app-feature-mailforwarding-circle.svg';
import movingAnnouncementOutline from '../../images/icons/app_features/icon-app-feature-moving-announcement-circle-outline.svg';
import movingAnnouncementNoCircle from '../../images/icons/app_features/icon-app-feature-moving-announcement.svg';
import movingAnnouncementCircle from '../../images/icons/app_features/icon-app-feature-moving-announcement-circle.svg';
import fsmOutline from '../../images/icons/app_features/icon-app-feature-reserve-a-mover-circle-outline.svg';
import fsmNoCircle from '../../images/icons/app_features/icon-app-feature-reserve-a-mover.svg';
import fsmCircle from '../../images/icons/app_features/icon-app-feature-reserve-a-mover-circle.svg';
import tvInternetOutline from '../../images/icons/app_features/icon-app-feature-tv-internet-circle-outline.svg';
import tvInternetNoCircle from '../../images/icons/app_features/icon-app-feature-tv-internet.svg';
import tvInternetCircle from '../../images/icons/app_features/icon-app-feature-tv-internet-circle.svg';
import utilitiesOutline from '../../images/icons/app_features/icon-app-feature-utilities-circle-outline.svg';
import utilitiesNoCircle from '../../images/icons/app_features/icon-app-feature-utilities.svg';
import utilitiesCircle from '../../images/icons/app_features/icon-app-feature-utilities-circle.svg';

// Partner Benefits
import customBrandingOutline from '../../images/icons/partner_benefits/icon-partner-benefit-custom-branding-circle-outline.svg';
import customBrandingNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-custom-branding.svg';
import customBrandingCircle from '../../images/icons/partner_benefits/icon-partner-benefit-custom-branding-circle.svg';
import customizationOutline from '../../images/icons/partner_benefits/icon-partner-benefit-customization-circle-outline.svg';
import customizationNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-customization.svg';
import customizationCircle from '../../images/icons/partner_benefits/icon-partner-benefit-customization-circle.svg';
import engagementMetricsOutline from '../../images/icons/partner_benefits/icon-partner-benefit-engagement-metrics-circle-outline.svg';
import engagementMetricsNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-engagement-metrics.svg';
import engagementMetricsCircle from '../../images/icons/partner_benefits/icon-partner-benefit-engagement-metrics-circle.svg';
import increasedAdoptionOutline from '../../images/icons/partner_benefits/icon-partner-benefit-increased-adoption-circle-outline.svg';
import increasedAdoptionNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-increased-adoption.svg';
import increasedAdoptionCircle from '../../images/icons/partner_benefits/icon-partner-benefit-increased-adoption-circle.svg';
import marketingResourcesOutline from '../../images/icons/partner_benefits/icon-partner-benefit-marketing-resources-circle-outline.svg';
import marketingResourcesNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-marketing-resources.svg';
import marketingResourcesCircle from '../../images/icons/partner_benefits/icon-partner-benefit-marketing-resources-circle.svg';
import preferredProviderOutline from '../../images/icons/partner_benefits/icon-partner-benefit-preferred-provider-circle-outline.svg';
import preferredProviderNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-preferred-provider.svg';
import preferredProviderCircle from '../../images/icons/partner_benefits/icon-partner-benefit-preferred-provider-circle.svg';
import residentPortalOutline from '../../images/icons/partner_benefits/icon-partner-benefit-resident-portal-circle-outline.svg';
import residentPortalNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-resident-portal.svg';
import residentPortalCircle from '../../images/icons/partner_benefits/icon-partner-benefit-resident-portal-circle.svg';
import solutionOutline from '../../images/icons/partner_benefits/icon-partner-benefit-solutions-circle-outline.svg';
import solutionNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-solutions.svg';
import solutionCircle from '../../images/icons/partner_benefits/icon-partner-benefit-solutions-circle.svg';
import successTeamOutline from '../../images/icons/partner_benefits/icon-partner-benefit-success-team-circle-outline.svg';
import successTeamNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-success-team.svg';
import successTeamCircle from '../../images/icons/partner_benefits/icon-partner-benefit-success-team-circle.svg';
import supportOutline from '../../images/icons/partner_benefits/icon-partner-benefit-support-circle-outline.svg';
import supportNoCircle from '../../images/icons/partner_benefits/icon-partner-benefit-support.svg';
import supportCircle from '../../images/icons/partner_benefits/icon-partner-benefit-support-circle.svg';

// Insurance Types
import insuranceAutoOutline from '../../images/icons/insurance_types/icon-insurance-auto-circle-outline.svg';
import insuranceAutoNoCircle from '../../images/icons/insurance_types/icon-insurance-auto.svg';
import insuranceAutoCircle from '../../images/icons/insurance_types/icon-insurance-auto-circle.svg';
import insuranceBoatOutline from '../../images/icons/insurance_types/icon-insurance-boat-circle-outline.svg';
import insuranceBoatNoCircle from '../../images/icons/insurance_types/icon-insurance-boat.svg';
import insuranceBoatCircle from '../../images/icons/insurance_types/icon-insurance-boat-circle.svg';
import insuranceHomeOutline from '../../images/icons/insurance_types/icon-insurance-home-circle-outline.svg';
import insuranceHomeNoCircle from '../../images/icons/insurance_types/icon-insurance-home.svg';
import insuranceHomeCircle from '../../images/icons/insurance_types/icon-insurance-home-circle.svg';
import insuranceIdentityTheftOutline from '../../images/icons/insurance_types/icon-insurance-identity-theft-circle-outline.svg';
import insuranceIdentityTheftCircle from '../../images/icons/insurance_types/icon-insurance-identity-theft-circle.svg';
import insuranceIdentityTheftNoCircle from '../../images/icons/insurance_types/icon-insurance-identity-theft.svg';
import insuranceLifeOutline from '../../images/icons/insurance_types/icon-insurance-life-circle-outline.svg';
import insuranceLifeCircle from '../../images/icons/insurance_types/icon-insurance-life-circle.svg';
import insuranceLifeNoCircle from '../../images/icons/insurance_types/icon-insurance-life.svg';
import insuranceMotorcycleOutline from '../../images/icons/insurance_types/icon-insurance-motorcycle-circle-outline.svg';
import insuranceMotorcycleCircle from '../../images/icons/insurance_types/icon-insurance-motorcycle-circle.svg';
import insuranceMotorcycleNoCircle from '../../images/icons/insurance_types/icon-insurance-motorcycle.svg';
import insurancePetOutline from '../../images/icons/insurance_types/icon-insurance-pet-circle-outline.svg';
import insurancePetCircle from '../../images/icons/insurance_types/icon-insurance-pet-circle.svg';
import insurancePetNoCircle from '../../images/icons/insurance_types/icon-insurance-pet.svg';
import insuranceRentersOutline from '../../images/icons/insurance_types/icon-insurance-renters-circle-outline.svg';
import insuranceRentersCircle from '../../images/icons/insurance_types/icon-insurance-renters-circle.svg';
import insuranceRentersNoCircle from '../../images/icons/insurance_types/icon-insurance-renters.svg';
import insuranceUmbrellaOutline from '../../images/icons/insurance_types/icon-insurance-umbrella-circle-outline.svg';
import insuranceUmbrellaCircle from '../../images/icons/insurance_types/icon-insurance-umbrella-circle.svg';
import insuranceUmbrellaNoCircle from '../../images/icons/insurance_types/icon-insurance-umbrella.svg';

// Business Verticals
import verticalsBanksOutline from '../../images/icons/biz_verticals/verticals-banks-circle-outline.svg';
import verticalsEntertainmentOutline from '../../images/icons/biz_verticals/verticals-entertainment-circle-outline.svg';
import verticalsGroceryOutline from '../../images/icons/biz_verticals/verticals-grocery-circle-outline.svg';
import verticalsHealthOutline from '../../images/icons/biz_verticals/verticals-health-circle-outline.svg';
import verticalsHomeServiceOutline from '../../images/icons/biz_verticals/verticals-home-service-circle-outline.svg';
import verticalsLocalBusinessOutline from '../../images/icons/biz_verticals/verticals-local-business-circle-outline.svg';
import verticalsPetOutline from '../../images/icons/biz_verticals/verticals-pet-circle-outline.svg';
import verticalsPharmacyOutline from '../../images/icons/biz_verticals/verticals-pharmacy-circle-outline.svg';
import verticalsRetailOutline from '../../images/icons/biz_verticals/verticals-retail-circle-outline.svg';
import verticalsTvInternetOutline from '../../images/icons/biz_verticals/verticals-tv-internet-circle-outline.svg';
import verticalsUtilityOutline from '../../images/icons/biz_verticals/verticals-utility-circle-outline.svg';
import verticalsBanksCircle from '../../images/icons/biz_verticals/verticals-banks-circle.svg';
import verticalsEntertainmentCircle from '../../images/icons/biz_verticals/verticals-entertainment-circle.svg';
import verticalsGroceryCircle from '../../images/icons/biz_verticals/verticals-grocery-circle.svg';
import verticalsHealthCircle from '../../images/icons/biz_verticals/verticals-health-circle.svg';
import verticalsHomeServiceCircle from '../../images/icons/biz_verticals/verticals-home-service-circle.svg';
import verticalsLocalBusinessCircle from '../../images/icons/biz_verticals/verticals-local-business-circle.svg';
import verticalsPetCircle from '../../images/icons/biz_verticals/verticals-pet-circle.svg';
import verticalsPharmacyCircle from '../../images/icons/biz_verticals/verticals-pharmacy-circle.svg';
import verticalsRetailCircle from '../../images/icons/biz_verticals/verticals-retail-circle.svg';
import verticalsTvInternetCircle from '../../images/icons/biz_verticals/verticals-tv-internet-circle.svg';
import verticalsUtilityCircle from '../../images/icons/biz_verticals/verticals-utility-circle.svg';
import verticalsBanksNoCircle from '../../images/icons/biz_verticals/verticals-banks.svg';
import verticalsEntertainmentNoCircle from '../../images/icons/biz_verticals/verticals-entertainment.svg';
import verticalsGroceryNoCircle from '../../images/icons/biz_verticals/verticals-grocery.svg';
import verticalsHealthNoCircle from '../../images/icons/biz_verticals/verticals-health.svg';
import verticalsHomeServiceNoCircle from '../../images/icons/biz_verticals/verticals-home-service.svg';
import verticalsLocalBusinessNoCircle from '../../images/icons/biz_verticals/verticals-local-business.svg';
import verticalsPetNoCircle from '../../images/icons/biz_verticals/verticals-pet.svg';
import verticalsPharmacyNoCircle from '../../images/icons/biz_verticals/verticals-pharmacy.svg';
import verticalsRetailNoCircle from '../../images/icons/biz_verticals/verticals-retail.svg';
import verticalsTvInternetNoCircle from '../../images/icons/biz_verticals/verticals-tv-internet.svg';
import verticalsUtilityNoCircle from '../../images/icons/biz_verticals/verticals-utility.svg';

// Employee Perks
import perksCommuterOutline from '../../images/icons/employee_perks/icon-employee-perks-commuter-circle-outline.svg';
import perksCultureOutline from '../../images/icons/employee_perks/icon-employee-perks-culture-circle-outline.svg';
import perksGivingBackOutline from '../../images/icons/employee_perks/icon-employee-perks-giving-back-circle-outline.svg';
import perksMealsOutline from '../../images/icons/employee_perks/icon-employee-perks-meals-circle-outline.svg';
import perksMedicalOutline from '../../images/icons/employee_perks/icon-employee-perks-medical-circle-outline.svg';
import perksOffsiteOutline from '../../images/icons/employee_perks/icon-employee-perks-offsite-circle-outline.svg';
import perksOwnershipOutline from '../../images/icons/employee_perks/icon-employee-perks-ownership-circle-outline.svg';
import perksPtoOutline from '../../images/icons/employee_perks/icon-employee-perks-pto-circle-outline.svg';
import perksWellnessOutline from '../../images/icons/employee_perks/icon-employee-perks-wellness-circle-outline.svg';
import perksCommuterCircle from '../../images/icons/employee_perks/icon-employee-perks-commuter-circle.svg';
import perksCultureCircle from '../../images/icons/employee_perks/icon-employee-perks-culture-circle.svg';
import perksGivingBackCircle from '../../images/icons/employee_perks/icon-employee-perks-giving-back-circle.svg';
import perksMealsCircle from '../../images/icons/employee_perks/icon-employee-perks-meals-circle.svg';
import perksMedicalCircle from '../../images/icons/employee_perks/icon-employee-perks-medical-circle.svg';
import perksOffsiteCircle from '../../images/icons/employee_perks/icon-employee-perks-offsite-circle.svg';
import perksOwnershipCircle from '../../images/icons/employee_perks/icon-employee-perks-ownership-circle.svg';
import perksPtoCircle from '../../images/icons/employee_perks/icon-employee-perks-pto-circle.svg';
import perksWellnessCircle from '../../images/icons/employee_perks/icon-employee-perks-wellness-circle.svg';
import perksCommuterNoCircle from '../../images/icons/employee_perks/icon-employee-perks-commuter.svg';
import perksCultureNoCircle from '../../images/icons/employee_perks/icon-employee-perks-culture.svg';
import perksGivingBackNoCircle from '../../images/icons/employee_perks/icon-employee-perks-giving-back.svg';
import perksMealsNoCircle from '../../images/icons/employee_perks/icon-employee-perks-meals.svg';
import perksMedicalNoCircle from '../../images/icons/employee_perks/icon-employee-perks-medical.svg';
import perksOffsiteNoCircle from '../../images/icons/employee_perks/icon-employee-perks-offsite.svg';
import perksOwnershipNoCircle from '../../images/icons/employee_perks/icon-employee-perks-ownership.svg';
import perksPtoNoCircle from '../../images/icons/employee_perks/icon-employee-perks-pto.svg';
import perksWellnessNoCircle from '../../images/icons/employee_perks/icon-employee-perks-wellness.svg';

// Jobs (departments)
import jobsCommunityOutline from '../../images/icons/job_openings/icon-job-community-circle-outline.svg';
import jobsEngOutline from '../../images/icons/job_openings/icon-job-eng-circle-outline.svg';
import jobsInternshipOutline from '../../images/icons/job_openings/icon-job-internship-circle-outline.svg';
import jobsOperationOutline from '../../images/icons/job_openings/icon-job-operation-circle-outline.svg';
import jobsProductOutline from '../../images/icons/job_openings/icon-job-product-circle-outline.svg';
import jobsSalesMarketingOutline from '../../images/icons/job_openings/icon-job-sales-marketing-circle-outline.svg';
import jobsCommunityCircle from '../../images/icons/job_openings/icon-job-community-circle.svg';
import jobsEngCircle from '../../images/icons/job_openings/icon-job-eng-circle.svg';
import jobsInternshipCircle from '../../images/icons/job_openings/icon-job-internship-circle.svg';
import jobsOperationCircle from '../../images/icons/job_openings/icon-job-operation-circle.svg';
import jobsProductCircle from '../../images/icons/job_openings/icon-job-product-circle.svg';
import jobsSalesMarketingCircle from '../../images/icons/job_openings/icon-job-sales-marketing-circle.svg';
import jobsCommunityNoCircle from '../../images/icons/job_openings/icon-job-community.svg';
import jobsEngNoCircle from '../../images/icons/job_openings/icon-job-eng.svg';
import jobsInternshipNoCircle from '../../images/icons/job_openings/icon-job-internship.svg';
import jobsOperationNoCircle from '../../images/icons/job_openings/icon-job-operation.svg';
import jobsProductNoCircle from '../../images/icons/job_openings/icon-job-product.svg';
import jobsSalesMarketingNoCircle from '../../images/icons/job_openings/icon-job-sales-marketing.svg';

const appFeatures = [
  {
    name: 'Account Update',
    icons: [accountUpdateNoCircle, accountUpdateCircle, accountUpdateOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715561/chrysalis-assets/icons/app-features/packages/icon-app-feature-account-update.zip',
  },
  {
    name: 'DIY Moving',
    icons: [diyMovingNoCircle, diyMovingCircle, diyMovingOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715596/chrysalis-assets/icons/app-features/packages/icon-app-feature-diy-moving.zip',
  },
  {
    name: 'Driver License',
    icons: [driverLicenseNoCircle, driverLicenseCircle, driverLicenseOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715649/chrysalis-assets/icons/app-features/packages/icon-app-feature-driver-license.zip',
  },
  {
    name: 'Full-Service Moving',
    icons: [fsmNoCircle, fsmCircle, fsmOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715718/chrysalis-assets/icons/app-features/packages/icon-app-feature-reserve-a-mover.zip',
  },
  {
    name: 'Home Security',
    icons: [homeSecurityNoCircle, homeSecurityCircle, homeSecurityOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715751/chrysalis-assets/icons/app-features/packages/icon-app-feature-home-security.zip',
  },
  {
    name: 'Insurance',
    icons: [insuranceNoCircle, insuranceCircle, insuranceOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716511/chrysalis-assets/icons/app-features/packages/icon-app-feature-insurance.zip',
  },
  {
    name: 'Exclusive Offers',
    icons: [
      exclusiveOffersNoCircle,
      exclusiveOffersCircle,
      exclusiveOffersOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716576/chrysalis-assets/icons/app-features/packages/icon-app-feature-exclusive-offers.zip',
  },
  {
    name: 'Local Guide',
    icons: [localGuideNoCircle, localGuideCircle, localGuideOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716608/chrysalis-assets/icons/app-features/packages/icon-app-feature-local-guide.zip',
  },
  {
    name: 'Local Services',
    icons: [localServicesNoCircle, localServicesCircle, localServicesOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716645/chrysalis-assets/icons/app-features/packages/icon-app-feature-local-services.zip',
  },
  {
    name: 'Mail Forwarding',
    icons: [
      mailforwardingNoCircle,
      mailforwardingCircle,
      mailforwardingOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716672/chrysalis-assets/icons/app-features/packages/icon-app-feature-mailforwarding.zip',
  },
  {
    name: 'Moving Announcement',
    icons: [
      movingAnnouncementNoCircle,
      movingAnnouncementCircle,
      movingAnnouncementOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716702/chrysalis-assets/icons/app-features/packages/icon-app-feature-moving-announcement.zip',
  },
  {
    name: 'Internet & TV',
    icons: [tvInternetNoCircle, tvInternetCircle, tvInternetOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716733/chrysalis-assets/icons/app-features/packages/icon-app-feature-tv-internet.zip',
  },
  {
    name: 'Utilities',
    icons: [utilitiesNoCircle, utilitiesCircle, utilitiesOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716774/chrysalis-assets/icons/app-features/packages/icon-app-feature-utilities.zip',
  },
];
const partnerBenefits = [
  {
    name: 'Custom Branding',
    icons: [
      customBrandingNoCircle,
      customBrandingCircle,
      customBrandingOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737467/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-custom-branding.zip',
  },
  {
    name: 'Customization',
    icons: [customizationNoCircle, customizationCircle, customizationOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737508/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-customization.zip',
  },
  {
    name: 'Engagement Metrics',
    icons: [
      engagementMetricsNoCircle,
      engagementMetricsCircle,
      engagementMetricsOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737538/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-engagement-metrics.zip',
  },
  {
    name: 'Increased Adoption',
    icons: [
      increasedAdoptionNoCircle,
      increasedAdoptionCircle,
      increasedAdoptionOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737563/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-increased-adoption.zip',
  },
  {
    name: 'Marketing Resources',
    icons: [
      marketingResourcesNoCircle,
      marketingResourcesCircle,
      marketingResourcesOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737598/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-marketing-resources.zip',
  },
  {
    name: 'Resident Portal',
    icons: [
      residentPortalNoCircle,
      residentPortalCircle,
      residentPortalOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737692/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-resident-portal.zip',
  },
  {
    name: 'Preferred Provider',
    icons: [
      preferredProviderNoCircle,
      preferredProviderCircle,
      preferredProviderOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737632/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-preferred-provider.zip',
  },
  {
    name: 'Solution',
    icons: [solutionNoCircle, solutionCircle, solutionOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737721/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-solutions.zip',
  },
  {
    name: 'Success Team',
    icons: [successTeamNoCircle, successTeamCircle, successTeamOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737744/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-success-team.zip',
  },
  {
    name: 'Support',
    icons: [supportNoCircle, supportCircle, supportOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737773/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-support.zip',
  },
];
const insuranceTypes = [
  {
    name: 'Auto',
    icons: [insuranceAutoNoCircle, insuranceAutoCircle, insuranceAutoOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738051/chrysalis-assets/icons/insurance-types/packages/icon-insurance-auto.zip',
  },
  {
    name: 'Boat',
    icons: [insuranceBoatNoCircle, insuranceBoatCircle, insuranceBoatOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738070/chrysalis-assets/icons/insurance-types/packages/icon-insurance-boat.zip',
  },
  {
    name: 'Home',
    icons: [insuranceHomeNoCircle, insuranceHomeCircle, insuranceHomeOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738091/chrysalis-assets/icons/insurance-types/packages/icon-insurance-home.zip',
  },
  {
    name: 'Identity Theft',
    icons: [
      insuranceIdentityTheftNoCircle,
      insuranceIdentityTheftCircle,
      insuranceIdentityTheftOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738112/chrysalis-assets/icons/insurance-types/packages/icon-insurance-identity-theft.zip',
  },
  {
    name: 'Life',
    icons: [insuranceLifeNoCircle, insuranceLifeCircle, insuranceLifeOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738139/chrysalis-assets/icons/insurance-types/packages/icon-insurance-life.zip',
  },
  {
    name: 'Motorcycle',
    icons: [
      insuranceMotorcycleNoCircle,
      insuranceMotorcycleCircle,
      insuranceMotorcycleOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738186/chrysalis-assets/icons/insurance-types/packages/icon-insurance-motorcycle.zip',
  },
  {
    name: 'Pet',
    icons: [insurancePetNoCircle, insurancePetCircle, insurancePetOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738212/chrysalis-assets/icons/insurance-types/packages/icon-insurance-pet.zip',
  },
  {
    name: 'Renters',
    icons: [
      insuranceRentersNoCircle,
      insuranceRentersCircle,
      insuranceRentersOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738234/chrysalis-assets/icons/insurance-types/packages/icon-insurance-renters.zip',
  },
  {
    name: 'Umbrella',
    icons: [
      insuranceUmbrellaNoCircle,
      insuranceUmbrellaCircle,
      insuranceUmbrellaOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738261/chrysalis-assets/icons/insurance-types/packages/icon-insurance-umbrella.zip',
  },
];
const bizVerticals = [
  {
    name: 'Banks',
    icons: [
      verticalsBanksNoCircle,
      verticalsBanksCircle,
      verticalsBanksOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738485/chrysalis-assets/icons/business-verticals/packages/verticals-banks.zip',
  },
  {
    name: 'Entertainment',
    icons: [
      verticalsEntertainmentNoCircle,
      verticalsEntertainmentCircle,
      verticalsEntertainmentOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738505/chrysalis-assets/icons/business-verticals/packages/verticals-entertainment.zip',
  },
  {
    name: 'Health',
    icons: [
      verticalsHealthNoCircle,
      verticalsHealthCircle,
      verticalsHealthOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738550/chrysalis-assets/icons/business-verticals/packages/verticals-health.zip',
  },
  {
    name: 'Home Services',
    icons: [
      verticalsHomeServiceNoCircle,
      verticalsHomeServiceCircle,
      verticalsHomeServiceOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738683/chrysalis-assets/icons/business-verticals/packages/verticals-home-service.zip',
  },
  {
    name: 'Local Business',
    icons: [
      verticalsLocalBusinessNoCircle,
      verticalsLocalBusinessCircle,
      verticalsLocalBusinessOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738707/chrysalis-assets/icons/business-verticals/packages/verticals-local-business.zip',
  },
  {
    name: 'Grocery',
    icons: [
      verticalsGroceryNoCircle,
      verticalsGroceryCircle,
      verticalsGroceryOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738527/chrysalis-assets/icons/business-verticals/packages/verticals-grocery.zip',
  },
  {
    name: 'Pet',
    icons: [verticalsPetNoCircle, verticalsPetCircle, verticalsPetOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738731/chrysalis-assets/icons/business-verticals/packages/verticals-pet.zip',
  },
  {
    name: 'Pharmacy',
    icons: [
      verticalsPharmacyNoCircle,
      verticalsPharmacyCircle,
      verticalsPharmacyOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738749/chrysalis-assets/icons/business-verticals/packages/verticals-pharmacy.zip',
  },
  {
    name: 'Retail',
    icons: [
      verticalsRetailNoCircle,
      verticalsRetailCircle,
      verticalsRetailOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738770/chrysalis-assets/icons/business-verticals/packages/verticals-retail.zip',
  },
  {
    name: 'Internet & TV',
    icons: [
      verticalsTvInternetNoCircle,
      verticalsTvInternetCircle,
      verticalsTvInternetOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738791/chrysalis-assets/icons/business-verticals/packages/verticals-tv-internet.zip',
  },
  {
    name: 'Utilities',
    icons: [
      verticalsUtilityNoCircle,
      verticalsUtilityCircle,
      verticalsUtilityOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738818/chrysalis-assets/icons/business-verticals/packages/verticals-utility.zip',
  },
];
const employeePerks = [
  {
    name: 'Commuter',
    noPadding: true,
    icons: [perksCommuterNoCircle, perksCommuterCircle, perksCommuterOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739473/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-commuter.zip',
  },
  {
    name: 'Culture',
    noPadding: true,
    icons: [perksCultureNoCircle, perksCultureCircle, perksCultureOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739493/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-culture.zip',
  },
  {
    name: 'Giving Back',
    noPadding: true,
    icons: [
      perksGivingBackNoCircle,
      perksGivingBackCircle,
      perksGivingBackOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739511/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-giving-back.zip',
  },
  {
    name: 'Meals',
    noPadding: true,
    icons: [perksMealsNoCircle, perksMealsCircle, perksMealsOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739531/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-meals.zip',
  },
  {
    name: 'Medical',
    noPadding: true,
    icons: [perksMedicalNoCircle, perksMedicalCircle, perksMedicalOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739554/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-medical.zip',
  },
  {
    name: 'Offsite',
    noPadding: true,
    icons: [perksOffsiteNoCircle, perksOffsiteCircle, perksOffsiteOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739574/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-offsite.zip',
  },
  {
    name: 'Ownership',
    noPadding: true,
    icons: [
      perksOwnershipNoCircle,
      perksOwnershipCircle,
      perksOwnershipOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739596/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-ownership.zip',
  },
  {
    name: 'PTO',
    noPadding: true,
    icons: [perksPtoNoCircle, perksPtoCircle, perksPtoOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739622/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-pto.zip',
  },
  {
    name: 'Wellness',
    noPadding: true,
    icons: [perksWellnessNoCircle, perksWellnessCircle, perksWellnessOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739643/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-wellness.zip',
  },
];
const jobDepartments = [
  {
    name: 'Talent Community',
    noPadding: true,
    icons: [jobsCommunityNoCircle, jobsCommunityCircle, jobsCommunityOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584740121/chrysalis-assets/icons/jobs-departments/packages/icon-job-community.zip',
  },
  {
    name: 'Engineering',
    noPadding: true,
    icons: [jobsEngNoCircle, jobsEngCircle, jobsEngOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584740140/chrysalis-assets/icons/jobs-departments/packages/icon-job-eng.zip',
  },
  {
    name: 'Operations',
    noPadding: true,
    icons: [jobsOperationNoCircle, jobsOperationCircle, jobsOperationOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584740187/chrysalis-assets/icons/jobs-departments/packages/icon-job-operation.zip',
  },
  {
    name: 'Internship',
    noPadding: true,
    icons: [
      jobsInternshipNoCircle,
      jobsInternshipCircle,
      jobsInternshipOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584740165/chrysalis-assets/icons/jobs-departments/packages/icon-job-internship.zip',
  },
  {
    name: 'Product',
    noPadding: true,
    icons: [jobsProductNoCircle, jobsProductCircle, jobsProductOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584740206/chrysalis-assets/icons/jobs-departments/packages/icon-job-product.zip',
  },
  {
    name: 'Marketing',
    noPadding: true,
    icons: [
      jobsSalesMarketingNoCircle,
      jobsSalesMarketingCircle,
      jobsSalesMarketingOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584740228/chrysalis-assets/icons/jobs-departments/packages/icon-job-sales-marketing.zip',
  },
];

export {
  appFeatures,
  partnerBenefits,
  insuranceTypes,
  bizVerticals,
  employeePerks,
  jobDepartments,
};
