/* eslint-disable camelcase */

// Update Addresses
import accountUpdateOutline from '../../images/icons/app_features/icon-app-feature-account-update-circle-outline.svg';
import accountUpdateNoCircle from '../../images/icons/app_features/icon-app-feature-account-update.svg';
import accountUpdateCircle from '../../images/icons/app_features/icon-app-feature-account-update-circle.svg';
import driverLicenseOutline from '../../images/icons/app_features/icon-app-feature-driver-license-circle-outline.svg';
import driverLicenseNoCircle from '../../images/icons/app_features/icon-app-feature-driver-license.svg';
import driverLicenseCircle from '../../images/icons/app_features/icon-app-feature-driver-license-circle.svg';
import mailforwardingOutline from '../../images/icons/app_features/icon-app-feature-mailforwarding-circle-outline.svg';
import mailforwardingNoCircle from '../../images/icons/app_features/icon-app-feature-mailforwarding.svg';
import mailforwardingCircle from '../../images/icons/app_features/icon-app-feature-mailforwarding-circle.svg';
import voterRegistrationOutline from '../../images/icons/feature_address_update/feature-details_voter-registration-outline.svg';
import voterRegistrationCircle from '../../images/icons/feature_address_update/feature-details_voter-registration-full-circle.svg';
import voterRegistrationNoCircle from '../../images/icons/feature_address_update/feature-details_voter-registration.svg';

// Preprare for Moving Day
import reserveOnlineOutline from '../../images/icons/feature_moving_prep/feature-details_schedule-online-outline.svg';
import reserveOnlineCircle from '../../images/icons/feature_moving_prep/feature-details_schedule-online-full-circle.svg';
import reserveOnline from '../../images/icons/feature_moving_prep/feature-details_schedule-online.svg';
import estimatesOnlineOutline from '../../images/icons/feature_moving_prep/feature-details_estimates-online-outline.svg';
import estimatesOnlineCircle from '../../images/icons/feature_moving_prep/feature-details_estimates-online-full-circle.svg';
import estimatesOnline from '../../images/icons/feature_moving_prep/feature-details_estimates-online.svg';
import truckRentalOutline from '../../images/icons/feature_moving_prep/feature-details_truck-rental-outline.svg';
import truckRentalCircle from '../../images/icons/feature_moving_prep/feature-details_truck-rental-full-circle.svg';
import truckRental from '../../images/icons/feature_moving_prep/feature-details_truck-rental.svg';
import hireLaborOutline from '../../images/icons/feature_moving_prep/feature-details_hire-labor-outline.svg';
import hireLaborCircle from '../../images/icons/feature_moving_prep/feature-details_hire-labor-full-circle.svg';
import hireLabor from '../../images/icons/feature_moving_prep/feature-details_hire-labor.svg';
import shipCarOutline from '../../images/icons/feature_moving_prep/feature-details_ship-a-car-outline.svg';
import shipCarCircle from '../../images/icons/feature_moving_prep/feature-details_ship-a-car-full-circle.svg';
import shipCar from '../../images/icons/feature_moving_prep/feature-details_ship-a-car.svg';
import junkRemovalOutline from '../../images/icons/feature_moving_prep/feature-details_junk-removal-outline.svg';
import junkRemovalCircle from '../../images/icons/feature_moving_prep/feature-details_junk-removal-full-circle.svg';
import junkRemoval from '../../images/icons/feature_moving_prep/feature-details_junk-removal.svg';

// Set Up Services
import tvInternetOutline from '../../images/icons/app_features/icon-app-feature-tv-internet-circle-outline.svg';
import tvInternetCircle from '../../images/icons/app_features/icon-app-feature-tv-internet-circle.svg';
import tvInternet from '../../images/icons/app_features/icon-app-feature-tv-internet.svg';
import insuranceOutline from '../../images/icons/app_features/icon-app-feature-insurance-circle-outline.svg';
import insuranceCircle from '../../images/icons/app_features/icon-app-feature-insurance-circle.svg';
import insurance from '../../images/icons/app_features/icon-app-feature-insurance.svg';
import utilitiesOutline from '../../images/icons/app_features/icon-app-feature-utilities-circle-outline.svg';
import utilitiesCircle from '../../images/icons/app_features/icon-app-feature-utilities-circle.svg';
import utilities from '../../images/icons/app_features/icon-app-feature-utilities.svg';
import waterOutline from '../../images/icons/feature_service_setup/feature-details_water-outline.svg';
import waterCircle from '../../images/icons/feature_service_setup/feature-details_water-full-circle.svg';
import water from '../../images/icons/feature_service_setup/feature-details_water.svg';
import gasOutline from '../../images/icons/feature_service_setup/feature-details_gas-outline.svg';
import gasCircle from '../../images/icons/feature_service_setup/feature-details_gas-full-circle.svg';
import gas from '../../images/icons/feature_service_setup/feature-details_gas.svg';

// Pack It Up
import buyBoxesOutline from '../../images/icons/feature_packing/feature-details_buy-boxes-outline.svg';
import buyBoxesCircle from '../../images/icons/feature_packing/feature-details_buy-boxes-full-circle.svg';
import buyBoxes from '../../images/icons/feature_packing/feature-details_buy-boxes.svg';
import checkListsOutline from '../../images/icons/feature_packing/feature-details_checklists-outline.svg';
import checkListsCircle from '../../images/icons/feature_packing/feature-details_checklists-full-circle.svg';
import checkLists from '../../images/icons/feature_packing/feature-details_checklists.svg';
import boxSizeOutline from '../../images/icons/feature_packing/feature-details_box-size-guide-outline.svg';
import boxSizeCircle from '../../images/icons/feature_packing/feature-details_box-size-guide-full-circle.svg';
import boxSize from '../../images/icons/feature_packing/feature-details_box-size-guide.svg';
import calculatorOutline from '../../images/icons/feature_packing/feature-details_calculator-outline.svg';
import calculatorCircle from '../../images/icons/feature_packing/feature-details_calculator-full-circle.svg';
import calculator from '../../images/icons/feature_packing/feature-details_calculator.svg';

// Personalize Home
import homeImprovementOutline from '../../images/icons/feature_personalize_home/feature-details_home-improvement-outline.svg';
import homeImprovementCircle from '../../images/icons/feature_personalize_home/feature-details_home-improvement-full-circle.svg';
import homeImprovement from '../../images/icons/feature_personalize_home/feature-details_home-improvement.svg';
import cleaningServicesOutline from '../../images/icons/feature_personalize_home/feature-details_cleaning-outline.svg';
import cleaningServicesCircle from '../../images/icons/feature_personalize_home/feature-details_cleaning-full-circle.svg';
import cleaningServices from '../../images/icons/feature_personalize_home/feature-details_cleaning.svg';
import smartHomeOutline from '../../images/icons/feature_personalize_home/feature-details_smart-home-automation-outline.svg';
import smartHomeCircle from '../../images/icons/feature_personalize_home/feature-details_smart-home-automation-full-circle.svg';
import smartHome from '../../images/icons/feature_personalize_home/feature-details_smart-home-automation.svg';
import homeSecurityOutline from '../../images/icons/app_features/icon-app-feature-home-security-circle-outline.svg';
import homeSecurityCircle from '../../images/icons/app_features/icon-app-feature-home-security-circle.svg';
import homeSecurity from '../../images/icons/app_features/icon-app-feature-home-security.svg';

// Save Money
import furnitureOutline from '../../images/icons/feature_save_money/feature-details_furniture-outline.svg';
import furnitureCircle from '../../images/icons/feature_save_money/feature-details_furniture-full-circle.svg';
import furniture from '../../images/icons/feature_save_money/feature-details_furniture.svg';
import groceryOutline from '../../images/icons/feature_save_money/feature-details_grocery-outline.svg';
import groceryCircle from '../../images/icons/feature_save_money/feature-details_grocery-full-circle.svg';
import grocery from '../../images/icons/feature_save_money/feature-details_grocery.svg';
import decorOutline from '../../images/icons/feature_save_money/feature-details_decor-outline.svg';
import decorCircle from '../../images/icons/feature_save_money/feature-details_decor-full-circle.svg';
import decor from '../../images/icons/feature_save_money/feature-details_decor.svg';
import appliancesOutline from '../../images/icons/feature_save_money/feature-details_appliances-outline.svg';
import appliancesCircle from '../../images/icons/feature_save_money/feature-details_appliances-full-circle.svg';
import appliances from '../../images/icons/feature_save_money/feature-details_appliances.svg';
import localBusinessOutline from '../../images/icons/feature_save_money/feature-details_local-business-outline.svg';
import localBusinessCircle from '../../images/icons/feature_save_money/feature-details_local-business-full-circle.svg';
import localBusiness from '../../images/icons/feature_save_money/feature-details_local-business.svg';

// Handle the Rest
import schoolInfoOutline from '../../images/icons/feature_misc/feature-details_school-info-outline.svg';
import schoolInfoCircle from '../../images/icons/feature_misc/feature-details_school-info-full-circle.svg';
import schoolInfo from '../../images/icons/feature_misc/feature-details_school-info.svg';
import petOutline from '../../images/icons/insurance_types/icon-insurance-pet-circle-outline.svg';
import petCircle from '../../images/icons/insurance_types/icon-insurance-pet-circle.svg';
import pet from '../../images/icons/insurance_types/icon-insurance-pet.svg';
import freightElevatorOutline from '../../images/icons/feature_misc/feature-details_freight-elevator-outline.svg';
import freightElevatorCircle from '../../images/icons/feature_misc/feature-details_freight-elevator-full-circle.svg';
import freightElevator from '../../images/icons/feature_misc/feature-details_freight-elevator.svg';
import handbookOutline from '../../images/icons/feature_misc/feature-details_community-handbook-outline.svg';
import handbookCircle from '../../images/icons/feature_misc/feature-details_community-handbook-full-circle.svg';
import handbook from '../../images/icons/feature_misc/feature-details_community-handbook.svg';
import eventsOutline from '../../images/icons/feature_misc/feature-details_neighborhood-events-outline.svg';
import eventsCircle from '../../images/icons/feature_misc/feature-details_neighborhood-events-full-circle.svg';
import events from '../../images/icons/feature_misc/feature-details_neighborhood-events.svg';

const featureAddressUpdate = [
  {
    name: 'Mail Forwarding',
    icons: [
      mailforwardingNoCircle,
      mailforwardingCircle,
      mailforwardingOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716672/chrysalis-assets/icons/app-features/packages/icon-app-feature-mailforwarding.zip',
  },
  {
    name: 'Update address everywhere',
    icons: [accountUpdateNoCircle, accountUpdateCircle, accountUpdateOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715561/chrysalis-assets/icons/app-features/packages/icon-app-feature-account-update.zip',
  },
  {
    name: 'Driver License',
    icons: [driverLicenseNoCircle, driverLicenseCircle, driverLicenseOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715649/chrysalis-assets/icons/app-features/packages/icon-app-feature-driver-license.zip',
  },
  {
    name: 'Voter Registration',
    noPadding: true,
    icons: [
      voterRegistrationNoCircle,
      voterRegistrationCircle,
      voterRegistrationOutline,
    ],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365010/chrysalis-assets/icons/feature-address-update/packages/feature-details_voter-registration.zip',
  },
];

const featureMovingPrep = [
  {
    name: 'Reserve a mover online',
    noPadding: true,
    icons: [reserveOnline, reserveOnlineCircle, reserveOnlineOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364313/chrysalis-assets/icons/feature-moving-prep/packages/feature-details_schedule-online.zip',
  },
  {
    name: 'Request moving estimates',
    noPadding: true,
    icons: [estimatesOnline, estimatesOnlineCircle, estimatesOnlineOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364313/chrysalis-assets/icons/feature-moving-prep/packages/feature-details_estimates-online.zip',
  },
  {
    name: 'Rent a truck',
    noPadding: true,
    icons: [truckRental, truckRentalCircle, truckRentalOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364313/chrysalis-assets/icons/feature-moving-prep/packages/feature-details_truck-rental.zip',
  },
  {
    name: 'Hire moving labor',
    noPadding: true,
    icons: [hireLabor, hireLaborCircle, hireLaborOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364313/chrysalis-assets/icons/feature-moving-prep/packages/feature-details_hire-labor.zip',
  },
  {
    name: 'Ship your car',
    noPadding: true,
    icons: [shipCar, shipCarCircle, shipCarOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364313/chrysalis-assets/icons/feature-moving-prep/packages/feature-details_ship-a-car.zip',
  },
  {
    name: 'Throw out your junk',
    noPadding: true,
    icons: [junkRemoval, junkRemovalCircle, junkRemovalOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364313/chrysalis-assets/icons/feature-moving-prep/packages/feature-details_junk-removal.zip',
  },
];

const featureSetUpServices = [
  {
    name: 'Internet & TV',
    icons: [tvInternet, tvInternetCircle, tvInternetOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716733/chrysalis-assets/icons/app-features/packages/icon-app-feature-tv-internet.zip',
  },
  {
    name: 'Insurance',
    icons: [insurance, insuranceCircle, insuranceOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716511/chrysalis-assets/icons/app-features/packages/icon-app-feature-insurance.zip',
  },
  {
    name: 'Electricity',
    icons: [utilities, utilitiesCircle, utilitiesOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716774/chrysalis-assets/icons/app-features/packages/icon-app-feature-utilities.zip',
  },
  {
    name: 'Water',
    noPadding: true,
    icons: [water, waterCircle, waterOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364456/chrysalis-assets/icons/feature-service-setup/packages/feature-details_water.zip',
  },
  {
    name: 'Gas',
    noPadding: true,
    icons: [gas, gasCircle, gasOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364456/chrysalis-assets/icons/feature-service-setup/packages/feature-details_gas.zip',
  },
];
const featurePacking = [
  {
    name: 'Buy Boxes',
    noPadding: true,
    icons: [buyBoxes, buyBoxesCircle, buyBoxesOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364586/chrysalis-assets/icons/feature-packing/packages/feature-details_buy-boxes.zip',
  },
  {
    name: 'Checklists',
    noPadding: true,
    icons: [checkLists, checkListsCircle, checkListsOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364586/chrysalis-assets/icons/feature-packing/packages/feature-details_checklists.zip',
  },
  {
    name: 'Box Size Guides',
    noPadding: true,
    icons: [boxSize, boxSizeCircle, boxSizeOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364586/chrysalis-assets/icons/feature-packing/packages/feature-details_box-size-guide.zip',
  },
  {
    name: 'Moving Calculator',
    noPadding: true,
    icons: [calculator, calculatorCircle, calculatorOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364586/chrysalis-assets/icons/feature-packing/packages/feature-details_calculator.zip',
  },
];

const featurePersonalizeHome = [
  {
    name: 'Home Improvement',
    noPadding: true,
    icons: [homeImprovement, homeImprovementCircle, homeImprovementOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364915/chrysalis-assets/icons/feature-personalize-home/packages/feature-details_home-improvement.zip',
  },
  {
    name: 'Cleaning Services',
    noPadding: true,
    icons: [cleaningServices, cleaningServicesCircle, cleaningServicesOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364915/chrysalis-assets/icons/feature-personalize-home/packages/feature-details_cleaning.zip',
  },
  {
    name: 'Smart Home Automation',
    noPadding: true,
    icons: [smartHome, smartHomeCircle, smartHomeOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364915/chrysalis-assets/icons/feature-personalize-home/packages/feature-details_smart-home-automation.zip',
  },
  {
    name: 'Home Security',
    icons: [homeSecurity, homeSecurityCircle, homeSecurityOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584715751/chrysalis-assets/icons/app-features/packages/icon-app-feature-home-security.zip',
  },
];
const featureSaveMoney = [
  {
    name: 'Furniture',
    noPadding: true,
    icons: [furniture, furnitureCircle, furnitureOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365151/chrysalis-assets/icons/feature-save-money/packages/feature-details_furniture.zip',
  },
  {
    name: 'Groceries',
    noPadding: true,
    icons: [grocery, groceryCircle, groceryOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365151/chrysalis-assets/icons/feature-save-money/packages/feature-details_grocery.zip',
  },
  {
    name: 'Decor',
    noPadding: true,
    icons: [decor, decorCircle, decorOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365151/chrysalis-assets/icons/feature-save-money/packages/feature-details_decor.zip',
  },
  {
    name: 'Appliances',
    noPadding: true,
    icons: [appliances, appliancesCircle, appliancesOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365151/chrysalis-assets/icons/feature-save-money/packages/feature-details_appliances.zip',
  },
  {
    name: 'Local Businesses',
    noPadding: true,
    icons: [localBusiness, localBusinessCircle, localBusinessOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365151/chrysalis-assets/icons/feature-save-money/packages/feature-details_local-business.zip',
  },
];
const featureMisc = [
  {
    name: 'Local Guides',
    noPadding: true,
    icons: [localBusiness, localBusinessCircle, localBusinessOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365151/chrysalis-assets/icons/feature-save-money/packages/feature-details_local-business.zip',
  },
  {
    name: 'School Information',
    noPadding: true,
    icons: [schoolInfo, schoolInfoCircle, schoolInfoOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365270/chrysalis-assets/icons/feature-misc/packages/feature-details_school-info.zip',
  },
  {
    name: 'Pet Requirements',
    icons: [pet, petCircle, petOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738212/chrysalis-assets/icons/insurance-types/packages/icon-insurance-pet.zip',
  },
  {
    name: 'Freight Elevator Booking',
    noPadding: true,
    icons: [freightElevator, freightElevatorCircle, freightElevatorOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365270/chrysalis-assets/icons/feature-misc/packages/feature-details_freight-elevator.zip',
  },
  {
    name: 'Community Handbook',
    noPadding: true,
    icons: [handbook, handbookCircle, handbookOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365270/chrysalis-assets/icons/feature-misc/packages/feature-details_community-handbook.zip',
  },
  {
    name: 'Neighborhood Events',
    noPadding: true,
    icons: [events, eventsCircle, eventsOutline],
    link:
      'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365270/chrysalis-assets/icons/feature-misc/packages/feature-details_neighborhood-events.zip',
  },
];
export {
  featureAddressUpdate,
  featureMovingPrep,
  featureSetUpServices,
  featurePacking,
  featurePersonalizeHome,
  featureSaveMoney,
  featureMisc,
};
