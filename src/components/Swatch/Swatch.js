import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ColorUtil from 'color';
import { copyToClipboard } from '../Utils/Utils';

const Card = styled.div`
  position: relative;
  cursor: pointer;
  display: inline-block;
  width: 180px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
  margin-right: 20px;
  margin-bottom: 20px;
  box-sizing: border-box;
  transition: transform 0.2s, box-shadow 0.2s;
  h3 {
    font-size: 12px;
    margin: 0;
  }
  p {
    font-size: 8px;
  }
  &:hover {
    transform: scale(1.03);
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.25);
  }
  &:last-of-type {
    margin-right: 0;
  }
  @media screen and (max-width: 1151px) {
    width: 150px;
  }
  @media screen and (max-width: 865px) {
    width: 120px;
  }
  @media screen and (max-width: 600px) {
    width: 100px;
    margin-right: 10px;
  }
  @media screen and (max-width: 501px) {
    width: 80px;
  }
  &.isMarco {
    width: 100px;
    margin-right: 10px;
    @media screen and (max-width: 501px) {
      width: 80px;
    }
  }
`;

const SwatchColor = styled.div`
  height: 130px;
  color: #fff;
  display: flex;
  font-family: var(--sans);
  font-weight: 200;
  justify-content: center;
  align-items: center;
  font-size: 1.8rem;
  span {
    opacity: 0;
    font-size: inherit;
    transition: opacity 0.2s, transform 0.2s;
    &.clicked {
      transform: scale(1.1);
      opacity: 1;
    }
  }
  @media screen and (max-width: 1151px) {
    height: 110px;
  }
  @media screen and (max-width: 865px) {
    height: 100px;
  }
  @media screen and (max-width: 600px) {
    font-size: 1.4rem;
    height: 90px;
  }
  @media screen and (max-width: 501px) {
    font-size: 1.2rem;
    height: 70px;
  }
  &.isMarco {
    font-size: 1.4rem;
    height: 90px;
    @media screen and (max-width: 501px) {
      font-size: 1.2rem;
      height: 70px;
    }
  }
`;

const SwatchText = styled.div`
  h3 {
    font-size: 1.5rem;
    font-weight: 400;
    margin-bottom: 0.5rem;
    line-height: 1;
    color: #fff;
  }
  box-sizing: border-box;
  padding: 20px 10px;
  color: #fff;
  width: 100%;
  flex-basis: 100%;
  line-height: 1.5;
  font-family: 'ars-maquette-web';
  font-weight: 400;
  text-align: center;
  span {
    opacity: 0.6;
    font-weight: 400;
    display: block;
    line-height: 1.5;
    font-size: 1.1rem;
  }
  .hexString {
    font-weight: 400;
  }
  @media screen and (max-width: 1151px) {
    h3 {
      font-size: 1.3rem;
    }
    span:not(.hexString) {
      display: none;
    }
  }
  @media screen and (max-width: 865px) {
    h3 {
      color: #555 !important;
      font-size: 1.2rem;
    }
    background: #fff;
    color: #555 !important;
    padding: 10px 0px;
  }
  @media screen and (max-width: 600px) {
    h3 {
      display: none;
    }
  }
`;

const Swatch = ({ name, hex, rgb, cmyk, isMarco }) => {
  const [isClicked, setIsClicked] = useState(false);
  const clickedSwatch = () => {
    copyToClipboard(hex);
    setIsClicked(true);
    setTimeout(() => {
      setIsClicked(false);
    }, 1000);
  };

  const darkerHex = ColorUtil(`#${hex}`)
    .darken(0.15)
    .rgb()
    .string();
  const infoTextColor =
    ColorUtil(`#${hex}`).hsl().color[2] > 70
      ? ColorUtil(`#${hex}`)
          .darken(0.8)
          .rgb()
          .string()
      : '#fff';

  return (
    <Card
      style={{ backgroundColor: darkerHex }}
      onClick={clickedSwatch}
      className={isMarco ? 'isMarco' : ''}
    >
      <SwatchColor
        style={{ backgroundColor: `#${hex}` }}
        className={isMarco ? 'isMarco' : ''}
      >
        <span
          className={isClicked ? 'clicked' : ''}
          style={{ color: infoTextColor }}
        >
          Copied!
        </span>
      </SwatchColor>

      <SwatchText style={{ color: infoTextColor }}>
        <h3 style={{ color: infoTextColor, display: isMarco ? 'none' : '' }}>
          {name}
        </h3>
        <span className="hexString">{`#${hex}`}</span>
        <span>
          {!isMarco ? 'RGB' : ''}: {rgb}
        </span>
        {!cmyk === false ? <span>CMYK: {cmyk}</span> : ''}
      </SwatchText>
    </Card>
  );
};

Swatch.propTypes = {
  name: PropTypes.string.isRequired, // Swatch name
  hex: PropTypes.string.isRequired, // HEX value in six characters
  rgb: PropTypes.string.isRequired,
  cmyk: PropTypes.string,
  isMarco: PropTypes.bool,
};

export default Swatch;
