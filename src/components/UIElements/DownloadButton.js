import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

const Button = styled.a`
  padding: 10px 20px;
  font-family: var(--sans);
  border-radius: 20px;
  font-size: 1.3rem;
  text-decoration: none;
  line-height: 1;
  background: var(--upd-yellow);
  // width: 140px;
  color: #fff;
  font-weight: 800;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  transition: background 0.2s, transform 0.2s;
  margin-left: 0.5rem;
  margin-right: 0.5rem;

  svg {
    width: 1.8rem;
    height: 2.2rem;
    margin-right: 1rem;
  }
  span {
    padding-left: 0.5rem;
    font-weight: 200;
  }
  &:hover {
    background: var(--upd-orange);
    transform: scale(1.05);
  }
`;

const DownloadButton = ({ href, filetype, optionalText }) => (
  <Button href={href} target="_blank" rel="noopener noreferrer">
    <svg
      viewBox="0 0 90 96"
      width="30"
      height="32"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="#FFF" fillRule="nonzero">
        <path d="M76 38L66 28 52 41V1H38v40L24 28 14 38l31 31z" />
        <path d="M75 62v19H15V62H0v34h90V62z" />
      </g>
    </svg>
    {filetype} <span>{optionalText}</span>
  </Button>
);

DownloadButton.propTypes = {
  href: PropTypes.string.isRequired,
  filetype: PropTypes.string.isRequired, // button label
  optionalText: PropTypes.string,
};

export default DownloadButton;
