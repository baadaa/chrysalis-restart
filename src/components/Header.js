import { Link } from 'gatsby';
import styled from 'styled-components';
import React from 'react';

const HeaderElem = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  background: #fff;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  z-index: 99;
  height: var(--header-height);
`;

const TopNav = styled.nav`
  font-family: var(--sans);
  max-width: 1200px;
  height: var(--header-height);
  display: flex;
  align-items: center;
  margin: 0 auto;
  a {
    text-decoration: none;
    color: inherit;
  }
`;

const LogoArea = styled.div`
  width: var(--sidebar-width);
  text-align: center;
  border-right: 1px solid #ddd;
  a {
    display: flex;
    align-items: center;
    transition: transform 0.2s;
    &:hover {
      transform: scale(1.051);
      svg,
      span {
        opacity: 1;
      }
    }
  }
  h6,
  p {
    margin: 0;
  }
  h6 {
    font-size: 2rem;
    margin-bottom: 0.3rem;
    color: var(--upd-light-blue);
  }
  p {
    font-size: 1.18rem;
    color: var(--upd-navy);
  }
  .chrysalisMark {
    color: rgba(255, 255, 255, 0.5);
    font-size: 2.5rem;
    display: flex;
    font-family: var(--sans);
    font-weight: 800;
    align-items: center;
    justify-content: center;
    content: '';
    width: 3.8rem;
    height: 3.8rem;
    animation: colorShift 60s infinite linear;
    margin-right: 1.2rem;
    margin-left: 1rem;
    border-radius: 5rem;
    svg {
      opacity: 0;
      transition: opacity 0.5s;
    }
    span {
      opacity: 0;
      transition: opacity 0.5s;
      transform: translateY(-0.45rem);
    }
  }
  @keyframes colorShift {
    0% {
      background: var(--upd-light-blue);
    }
    10% {
      background: var(--upd-light-green);
    }
    20% {
      background: var(--upd-turquoise);
    }
    30% {
      background: var(--upd-yellow);
    }
    40% {
      background: var(--upd-orange);
    }
    50% {
      background: var(--upd-pink);
    }
    60% {
      background: var(--upd-red);
    }
    70% {
      background: var(--upd-blue);
    }
    80% {
      background: var(--upd-navy);
    }
    100% {
      background: var(--upd-light-blue);
    }
  }
`;
const MonogramU = () => (
  <svg
    width="33"
    height="33"
    data-name="Layer 1"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 104.7 104.7"
  >
    <path
      d="M61.2 55.6c0 6.3-4.3 10.6-10.4 10.6-5 0-7.3-2.6-7.3-8.6V27.8H30v32C30 71.6 37 79 48 79a20 20 0 0013.3-5v4h13.4V27.7H61.2z"
      fill="#fff"
    />
  </svg>
);

const SearchBar = styled.input`
  border: 1px solid #eee;
  align-self: stretch;
  margin: 1.5rem;
  font-size: 1.5rem;
  font-weight: 200;
  box-sizing: border-box;
  padding: 0.5rem 1rem;
  flex: 1;
`;
const Header = () => (
  <HeaderElem>
    <TopNav>
      <LogoArea>
        <Link to="/">
          <div className="chrysalisMark">
            {/* <span>u</span> */}
            <MonogramU />
          </div>
          <div className="chrysalisName">
            <h6>chrysalis</h6>
            <p>Updater Design</p>
          </div>
        </Link>
      </LogoArea>
      <SearchBar placeholder="Search..." />
    </TopNav>
  </HeaderElem>
);

export default Header;
