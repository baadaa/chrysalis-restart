import React, { useState } from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

const SidebarElem = styled.nav`
  width: var(--sidebar-width);
  background: var(--upd-navy);
  padding-top: 2rem;
  // position: relative;
  & > ul {
    width: var(--sidebar-width);
    position: fixed;
  }
  ul {
    font-size: 1.5rem;
    list-style: none;
    padding: 0;
    margin: 0;
    font-family: var(--sans);
    color: #fff;
    a.topLevel {
      text-decoration: none;
      color: inherit;
      flex: 1;
      display: block;
      padding: 1.5rem 1.5rem 1.5rem 2rem;
      border-left: 5px solid transparent;
      transition: background 0.2s, border-color 0.2s;
      &:hover {
        background: var(--upd-blue);
      }
      &:hover {
        border-left-color: var(--upd-blue);
      }
      &.active {
        border-left-color: var(--upd-light-blue);
      }
    }
    a.subLevel {
      text-decoration: none;
      color: inherit;
      flex: 1;
      display: block;
      padding: 0.5rem 0.5rem 1rem 2rem;
      line-height: 1;
      transition: background 0.3s, border-color 0.3s;
      &:hover {
        background: var(--upd-blue);
      }
      &.active {
        border-left-color: var(--upd-light-blue);
      }
      &:hover {
        border-left-color: var(--upd-blue);
      }
    }
    li {
      box-sizing: border-box;
      display: flex;
      margin-bottom: 0;
      flex-direction: column;
      justify-content: stretch;
      align-items: stretch;
    }
    li > ul {
      display: none;
      font-size: 1.3rem;
      a {
        display: flex;
        align-items: center;
        line-height: 1.3rem;
        &::before {
          content: '\\25b6';
          font-size: 0.8rem;
          margin-right: 0.5rem;
        }
        &.activeSub::before {
          color: var(--upd-yellow);
        }
      }
    }
    a.active + ul {
      display: block;
      border-left: 5px solid var(--upd-light-blue);
      // background: rgba(255, 255, 255, 0.1);
      padding-bottom: 1rem;
    }
  }
  transition: transform 0.2s;
  &.isExpanded {
    transform: translateX(0);
    box-shadow: 2px 0 10px rgba(0, 0, 0, 0.3);
  }
  @media screen and (max-width: 910px) {
    position: fixed;
    transform: translateX(calc(0px - var(--sidebar-width)));
    top: var(--header-height);
    left: 0;
    bottom: 0;
    z-index: 9;
  }
`;

const ToggleBtn = styled.div`
  position: fixed;
  z-index: 9;
  right: 2rem;
  top: 10rem;
  box-sizing: border-box;
  border: none;
  display: none;
  button {
    box-sizing: border-box;
    background-color: #fff;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    border-radius: 4rem;
    width: 4rem;
    height: 4rem;
    opacity: 0.6;
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3E%3Cpath style='block-progression:tb' fill='%2339b3ca' d='M13 16a6 6 0 100 12h74a6 6 0 000-12zm0 28a6 6 0 100 12h74a6 6 0 000-12zm0 28a6 6 0 100 12h74a6 6 0 000-12z' overflow='visible' /%3E%3C/svg%3E");
    background-size: 60%;
    background-position: center;
    background-repeat: no-repeat;
    border: 1px solid var(--upd-light-blue);
    outline: none;
    transition: transform 0.2s, opacity 0.2s;
    cursor: pointer;
    &:hover {
      transform: scale(1.1);
      opacity: 1;
    }
  }
  @media screen and (max-width: 910px) {
    display: block;
  }
`;

const Sidebar = () => {
  const [isCollapsed, setIsCollapsed] = useState(true);
  return (
    <>
      <ToggleBtn>
        <button type="button" onClick={() => setIsCollapsed(!isCollapsed)}>
          &nbsp;
        </button>
      </ToggleBtn>
      <SidebarElem className={isCollapsed ? '' : 'isExpanded'}>
        <ul>
          <li>
            <Link
              onClick={() => setIsCollapsed(true)}
              className="topLevel"
              activeClassName="active"
              to="/fundamentals"
            >
              Fundamentals
            </Link>
          </li>
          <li>
            <Link
              className="topLevel"
              activeClassName="active"
              to="/design-tokens"
              getProps={({ isPartiallyCurrent }) =>
                isPartiallyCurrent ? { className: 'topLevel active' } : null
              }
            >
              Design Tokens
            </Link>
            <ul>
              <li>
                <Link
                  onClick={() => setIsCollapsed(true)}
                  className="subLevel"
                  activeClassName="activeSub"
                  to="/design-tokens/logos"
                >
                  Logos
                </Link>
              </li>
              <li>
                <Link
                  onClick={() => setIsCollapsed(true)}
                  className="subLevel"
                  activeClassName="activeSub"
                  to="/design-tokens/colors"
                >
                  Colors
                </Link>
              </li>
              <li>
                <Link
                  onClick={() => setIsCollapsed(true)}
                  className="subLevel"
                  activeClassName="activeSub"
                  to="/design-tokens/typography"
                >
                  Typography
                </Link>
              </li>
              <li>
                <Link
                  onClick={() => setIsCollapsed(true)}
                  className="subLevel"
                  activeClassName="activeSub"
                  to="/design-tokens/icons"
                >
                  Icons
                </Link>
              </li>
              <li>
                <Link
                  onClick={() => setIsCollapsed(true)}
                  className="subLevel"
                  activeClassName="activeSub"
                  to="/design-tokens/style-examples"
                >
                  Style Examples
                </Link>
              </li>
            </ul>
          </li>
          <li>
            <Link
              onClick={() => setIsCollapsed(true)}
              className="topLevel"
              activeClassName="active"
              to="/voice-and-tone"
            >
              Voice & Tone
            </Link>
          </li>
          <li>
            <Link
              onClick={() => setIsCollapsed(true)}
              className="topLevel"
              activeClassName="active"
              to="/internal-documents"
            >
              Internal Documents
            </Link>
          </li>
        </ul>
      </SidebarElem>
    </>
  );
};

export default Sidebar;
