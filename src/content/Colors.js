import React from 'react';
import styled from 'styled-components';
import SEO from '../components/seo';
import Swatch from '../components/Swatch/Swatch';

import DownloadButton from '../components/UIElements/DownloadButton';

const OverviewGrid = styled.div`
  display: grid;
  max-width: 80rem;
  grid-template-columns: repeat(36, 1fr);
  grid-template-rows: 12rem 19rem 4rem;
  grid-row-gap: 0.5rem;
`;

const OverviewGridItem = styled.div`
  background: ${props => props.bg};
  grid-column: ${props => props.columns};
  display: flex;
  box-sizing: border-box;
  color: #fff;
  position: relative;
  font-family: var(--sans);
  align-items: center;
  overflow: visible;
  transition: transform 0.2s;
  span {
    position: absolute;
    top: 0;
    bottom: 0.75rem;
    width: 150px;
    z-index: 3;
    margin-left: 2rem;
    font-size: 1.3rem;
    display: flex;
    align-items: flex-end;
    font-weight: 800;
  }
`;

const ColorsPage = () => (
  <div>
    <SEO title="Colors" />
    <h2>Colors</h2>
    <br />
    <h3>At a Glance</h3>
    <OverviewGrid>
      <OverviewGridItem bg="var(--upd-pink)" columns="span 30">
        <span>Impact Colors</span>
      </OverviewGridItem>
      <OverviewGridItem bg="var(--upd-red)" columns="span 3" />
      <OverviewGridItem bg="var(--upd-black)" columns="span 3" />
      <OverviewGridItem bg="var(--upd-navy)" columns="span 6">
        <span>Driving Colors</span>
      </OverviewGridItem>
      <OverviewGridItem bg="var(--upd-blue)" columns="span 12" />
      <OverviewGridItem bg="var(--upd-light-blue)" columns="span 18" />
      <OverviewGridItem bg="var(--upd-light-green)" columns="span 9">
        <span>Support Colors</span>
      </OverviewGridItem>
      <OverviewGridItem bg="var(--upd-turquoise)" columns="span 9" />
      <OverviewGridItem bg="var(--upd-yellow)" columns="span 9" />
      <OverviewGridItem bg="var(--upd-orange)" columns="span 9" />
    </OverviewGrid>
    <br />
    <br />
    <hr />
    <h3>Impact Colors</h3>
    <p>
      The defining colors of Updater brand. Only for key highlight and accent,
      and not to be used as a large background.
    </p>
    <br />
    <Swatch
      hex="f5333f"
      cmyk="0/93/75/0"
      rgb="245/51/63"
      name="Energetic Pink"
    />
    <Swatch
      hex="8e1b26"
      cmyk="27/100/87/28"
      rgb="142/27/38"
      name="Danger Red"
    />
    <Swatch hex="202020" cmyk="0/0/0/87" rgb="32/32/32" name="Sensible Black" />
    <br />
    <br />
    <h3>Driving Colors</h3>
    <p>
      Lead top-line content elements with these, such as heading, subheads, or
      chart titles. They are foundational for most impactful or Red tints.
    </p>
    <br />
    <Swatch hex="334d5c" cmyk="82/60/47/30" rgb="51/77/92" name="Calm Navy" />
    <Swatch
      hex="2b7499"
      cmyk="84/47/24/3"
      rgb="43/116/153"
      name="Serene Blue"
    />
    <Swatch
      hex="39b3ca"
      cmyk="69/8/18/0"
      rgb="57/179/202"
      name="Hopeful Light Blue"
    />
    <br />
    <br />
    <h3>Support Colors</h3>
    <p>
      Only auxiliary and tertiary. Not intended to be a leading hue that
      dominates the visual. Reserved mostly for charts/data, or otherwise
      aesthetic compositions.
    </p>
    <br />
    <Swatch
      hex="81be62"
      cmyk="54/3/75/0"
      rgb="129/190/110"
      name="Positive Green"
    />
    <Swatch
      hex="45b19c"
      cmyk="70/7/47/0"
      rgb="69/177/156"
      name="Relaxed Turquoise"
    />
    <Swatch
      hex="f2a328"
      cmyk="3/40/93/0"
      rgb="242/163/40"
      name="Sunny Yellow"
    />
    <Swatch
      hex="e27b3e"
      cmyk="8/62/86/0"
      rgb="226/123/62"
      name="Careful Orange"
    />
    <br />
    <div
      style={{
        padding: '2rem',
        background: '#f0f0f0',
        display: 'flex',
        maxWidth: '80rem',
        alignItems: 'center',
        flexWrap: 'wrap',
      }}
    >
      <h4 style={{ lineHeight: 1, margin: 0 }}>Download:</h4>
      <DownloadButton
        href="https://res.cloudinary.com/updater-marketing/raw/upload/v1559750711/chrysalis-assets/colors/updater-colors-rgb.ase"
        filetype=".ASE"
        optionalText="(RGB)"
      />
      <DownloadButton
        href="https://res.cloudinary.com/updater-marketing/raw/upload/v1559750711/chrysalis-assets/colors/updater-colors-cmyk.ase"
        filetype=".ASE"
        optionalText="(CMYK)"
      />
      <DownloadButton
        href="https://res.cloudinary.com/updater-marketing/raw/upload/v1559751597/chrysalis-assets/colors/updater-colors.sketch"
        filetype=".Sketch"
      />
    </div>
    <br />
    <br />
    <br />
    <hr />
    <h2>Marco</h2>
    <h3
      style={{
        fontWeight: 200,
        fontSize: '2rem',
        color: '#f5333f',
        marginTop: '-5px',
      }}
    >
      Legacy Colors
    </h3>
    <p>
      The palette still in use in Mover App, containing four primary colors,
      each respectively with four secondary tints. For UX consistency, as of
      June 2019, we stick to this palette for any design task within the App.
      Any external communications&mdash;investor materials, branded materials,
      white papers, etc&mdash;should use the primary colors above.
    </p>
    <br />
    <Swatch isMarco hex="2b7499" rgb="43/116/153" name="Jelly Bean" />
    <Swatch isMarco hex="5590ad" rgb="85/144/173" name="Air Force Blue" />
    <Swatch isMarco hex="80acc2" rgb="128/172/194" name="Nepal" />
    <Swatch isMarco hex="aac7d6" rgb="170/199/214" name="Pastel Blue" />
    <Swatch isMarco hex="d5e3eb" rgb="213/227/235" name="Pattens Blue" />
    <br />
    <Swatch isMarco hex="56b784" rgb="86/183/132" name="Forest Green" />
    <Swatch isMarco hex="75c79b" rgb="117/199/155" name="Summer Green" />
    <Swatch isMarco hex="92d7b2" rgb="146/215/178" name="Turquoise Green" />
    <Swatch isMarco hex="b1e7c9" rgb="177/231/201" name="Granny Apple" />
    <Swatch isMarco hex="e3f4eb" rgb="227/244/235" name="Bubbles" />
    <br />
    <Swatch isMarco hex="f84e57" rgb="248/78/87" name="Roman" />
    <Swatch isMarco hex="f97179" rgb="249/113/121" name="Geraldine" />
    <Swatch isMarco hex="fb959a" rgb="251/149/154" name="Sea Pink" />
    <Swatch isMarco hex="fcb8bc" rgb="252/184/188" name="Beauty Bush" />
    <Swatch isMarco hex="fee8e9" rgb="254/232/233" name="Amour" />
    <br />
    <Swatch isMarco hex="ffe98f" rgb="255/233/143" name="Drover" />
    <Swatch isMarco hex="ffefad" rgb="255/239/173" name="Buttermilk" />
    <Swatch isMarco hex="fff7db" rgb="255/247/219" name="Half Dutch White" />
    <Swatch isMarco hex="fffcec" rgb="255/252/236" name="Island Spice" />
    <Swatch isMarco hex="fffdf5" rgb="255/253/245" name="Black White" />
    <br />
  </div>
);

export default ColorsPage;
