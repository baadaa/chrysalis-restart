import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import SEO from '../components/seo';
import {
  appFeatures,
  partnerBenefits,
  insuranceTypes,
  bizVerticals,
  employeePerks,
  jobDepartments,
} from '../components/IconDefs/IconDefs';
import {
  featureAddressUpdate,
  featureMovingPrep,
  featureSetUpServices,
  featurePacking,
  featurePersonalizeHome,
  featureSaveMoney,
  featureMisc,
} from '../components/IconDefs/FeatureDetailIcons';
import { randomString } from '../components/Utils/Utils';
import DownloadButton from '../components/UIElements/DownloadButton';

const IconTable = styled.div`
  display: table;
  border: 1px solid #eee;
  border-radius: 1rem;
  font-family: var(--sans);
  flex-wrap: wrap;
  box-shadow: 1px 2px 10px rgba(0, 0, 0, 0.2);
  @media screen and (max-width: 600px) {
    width: 100%;
    max-width: 320px;
  }
  .iconRow {
    display: table-row;
    & > * {
      padding: 1rem 2rem;
      vertical-align: middle;
      display: table-cell;
    }
    &:nth-of-type(2n) {
      background: #f0f9fe;
    }
    @media screen and (max-width: 600px) {
      display: table;
      width: 100%;
      text-align: center;
      padding-top: 1rem;
      padding-bottom: 0.5rem;
    }
  }
  .iconName {
    padding-left: 2rem;
    padding-right: 2rem;
    font-size: 1.5rem;
    font-weight: 200;
    @media screen and (max-width: 600px) {
      padding: 1rem;
      display: table-row;
    }
  }
  .iconGraphics {
    svg {
      width: 5rem;
      height: 5rem;
      transition: transform 0.2s;
      &:hover {
        transform: scale(1.07);
      }
      &:first-of-type {
        transform: scale(0.75);
        &:hover {
          transform: scale(0.85);
        }
      }
      &.noPad:first-of-type {
        transform: scale(1.3);
        &:hover {
          transform: scale(1.5);
        }
      }
    }
    svg + svg {
      margin-left: 1rem;
    }
    @media screen and (max-width: 600px) {
      svg {
        width: 4rem;
        height: 4rem;
      }
    }
  }
  .iconLink {
    font-size: 1.2rem;
    font-weight: 400;
    a {
      text-decoration: none;
      padding: 0.5rem 1rem;
      background: var(--upd-yellow);
      line-height: 1;
      color: #fff;
      border-radius: 2rem;
      transition: background 0.2s;
      &:hover {
        background: var(--upd-orange);
      }
    }
    @media screen and (max-width: 600px) {
      display: none;
    }
  }
`;

const IconRow = ({ iconSet }) => {
  const IconList = iconSet.icons.map(icon => (
    <svg key={icon.id} className={iconSet.noPadding ? 'noPad' : ''}>
      <use xlinkHref={`#${icon.id}`} />
    </svg>
  ));
  return (
    <div className="iconRow" key={iconSet.name + randomString()}>
      <div className="iconName">{iconSet.name}</div>
      <div className="iconGraphics">
        <a href={iconSet.link} target="_blank" rel="noopener noreferrer">
          {IconList}
        </a>
      </div>
      <div className="iconLink">
        <a href={iconSet.link} target="_blank" rel="noopener noreferrer">
          Download
        </a>
      </div>
    </div>
  );
};
const TableTemplate = ({ iconCollection, downloadInfo }) => (
  <>
    <IconTable>
      {iconCollection.map(iconSet => (
        <IconRow
          key={iconSet.name}
          iconSet={iconSet}
          noPadding={iconSet.noPadding}
        />
      ))}
    </IconTable>
    <br />
    <DownloadButton href={downloadInfo.href} filetype={downloadInfo.filetype} />
  </>
);
const IconsPage = () => (
  <>
    <SEO title="Icons" />
    <h2>Icons</h2>
    <br />
    <h3>App Features</h3>
    <p>Core mover app features.</p>
    <br />
    <TableTemplate
      iconCollection={appFeatures}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1584716860/chrysalis-assets/icons/app-features/packages/icon-app-feature-collection.zip',
        filetype: 'App Feature Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <hr />
    <br />
    <br />
    <h3>Features Full List</h3>
    <p>Updater features as listed on the website.</p>
    <br />
    <h4>Prepare for Moving Day</h4>
    <TableTemplate
      iconCollection={featureMovingPrep}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364523/chrysalis-assets/icons/feature-moving-prep/packages/feature-moving-prep.zip',
        filetype: 'Moving Prep Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <h4>Set up Services</h4>
    <TableTemplate
      iconCollection={featureSetUpServices}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364487/chrysalis-assets/icons/feature-service-setup/packages/feature-service-setup.zip',
        filetype: 'Service Setup Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <h4>Pack It Up</h4>
    <TableTemplate
      iconCollection={featurePacking}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364586/chrysalis-assets/icons/feature-packing/packages/feature-packing.zip',
        filetype: 'Packing Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <h4>Personalize Your Home</h4>
    <TableTemplate
      iconCollection={featurePersonalizeHome}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1588364915/chrysalis-assets/icons/feature-personalize-home/packages/feature-personalize-home.zip',
        filetype: 'Home Personalization Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <h4>Update Your Address</h4>
    <TableTemplate
      iconCollection={featureAddressUpdate}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365010/chrysalis-assets/icons/feature-address-update/packages/feature-address-update.zip',
        filetype: 'Update Address Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <h4>Save Money</h4>
    <TableTemplate
      iconCollection={featureSaveMoney}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365151/chrysalis-assets/icons/feature-save-money/packages/feature-save-money.zip',
        filetype: 'Save Money Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <h4>Miscellaneous</h4>
    <TableTemplate
      iconCollection={featureMisc}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1588365270/chrysalis-assets/icons/feature-misc/packages/features-misc.zip',
        filetype: 'Miscellaneous Features Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <hr />
    <br />
    <h3>Partner Benefits</h3>
    <p>Values Updater mover app brings to the real estate partners</p>
    <br />
    <TableTemplate
      iconCollection={partnerBenefits}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1584737816/chrysalis-assets/icons/partner-benefits/packages/icon-partner-benefit-collection.zip',
        filetype: 'Partner Benefits Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <hr />
    <br />
    <h3>Insurance Types</h3>
    <p>
      Insurance policy types that can potentially be purchased via mover app
    </p>
    <br />
    <TableTemplate
      iconCollection={insuranceTypes}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1584738321/chrysalis-assets/icons/insurance-types/packages/icon-insurance-collection.zip',
        filetype: 'Insurance Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <hr />
    <br />
    <h3>Business Verticals (for partnership)</h3>
    <p>Industry areas that can partner with Updater</p>
    <br />
    <TableTemplate
      iconCollection={bizVerticals}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739294/chrysalis-assets/icons/business-verticals/packages/icon-business-vertical-package.zip',
        filetype: 'Biz Vertical Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <hr />
    <br />
    <h3>Employee Perks</h3>
    <p>(internal use only) Highlights of employee benefits</p>
    <br />
    <TableTemplate
      iconCollection={employeePerks}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1584739689/chrysalis-assets/icons/employee-perks/packages/icon-employee-perks-package.zip',
        filetype: 'Perks Icon Package',
      }}
    />
    <br />
    <br />
    <br />
    <hr />
    <br />
    <h3>Job Openings (departments)</h3>
    <p>(internal use only) Departments within Updater Inc.</p>
    <br />
    <TableTemplate
      iconCollection={jobDepartments}
      downloadInfo={{
        href:
          'https://res.cloudinary.com/updater-marketing/raw/upload/v1584740264/chrysalis-assets/icons/jobs-departments/packages/icon-job-departments-package.zip',
        filetype: 'Jobs Icon Package',
      }}
    />
    <br />
  </>
);

TableTemplate.propTypes = {
  iconCollection: PropTypes.object.isRequired,
  downloadInfo: {
    href: PropTypes.string.isRequired,
    filetype: PropTypes.string.isRequired,
  },
};

IconRow.propTypes = {
  iconSet: PropTypes.object.isRequired,
};
export default IconsPage;
