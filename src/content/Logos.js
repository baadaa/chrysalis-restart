import React from 'react';
import styled from 'styled-components';
import SEO from '../components/seo';
import UpdLogo from '../images/logos/updater_logo.svg';
import UpdLogoBk from '../images/logos/updater_logo_bk.svg';
import UpdLogoKo from '../images/logos/updater_logo_white.svg';
import UpdMonogram from '../images/logos/updater_U_logo.svg';
import UpdMonogramInverse from '../images/logos/updater_U_logo_inverse_border.svg';
import UpdMonogramKo from '../images/logos/updater_U_logo_ko.svg';

import UhsLogo from '../images/logos/updater-home-services-logo_r2.svg';
import MhqLogo from '../images/logos/movehq_main.svg';
import MhqLogoKo from '../images/logos/movehq_main_ko.svg';
import MhqLogoKoHighlight from '../images/logos/movehq_main_ko_highlight.svg';
import MhqLogoTagline from '../images/logos/movehq_main_tagline2.svg';
import MhqLogoTaglineKo from '../images/logos/movehq_main_tagline2_ko.svg';
import MhqLogoTaglineKoHighlight from '../images/logos/movehq_main_tagline2_ko_highlight.svg';
import MhqMonogram from '../images/logos/hq_main.svg';
import MhqMonogramKo from '../images/logos/hq_main_ko.svg';
import DownloadButton from '../components/UIElements/DownloadButton';

const LogoSummary = styled.div`
  margin: 3rem auto;
  text-align: center;
  padding: 3rem;
  border-radius: 2rem;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.1);
  background: #e8eff2;
  h3 {
    color: #202020;
    font-size: 3rem;
    font-weight: 200;
    margin-bottom: 3rem;
  }
  .logoOverview {
    font-size: 3rem;
    display: flex;
    justify-content: center;
    align-items: center;
    span {
      padding: 0 1rem;
    }
  }
  svg {
    border-radius: 2rem;
    width: 30%;
    background: #fff;
    flex-basis: 30%;
    height: 80px;
    padding: 2rem;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
  }
  @media screen and (max-width: 768px) {
    h3 {
      font-size: 2.3rem;
    }
    svg {
      padding-left: 3rem;
      padding-right: 3rem;
    }
  }
  @media screen and (max-width: 640px) {
    svg {
      padding: 2rem;
    }
  }
  @media screen and (max-width: 550px) {
    .logoOverview {
      flex-direction: column;
      justify-content: flex-start;
      span {
        padding: 1rem;
      }
    }
    svg {
      width: 150px;
      height: 50px;
      padding: 1.2rem 0;
      flex-basis: auto;
    }
  }
`;

const LogoSet = styled.div`
  display: grid;
  max-width: 1200px;
  margin-top: 2rem;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 2rem;
  .LogoItem {
    box-sizing: border-box;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 2.5rem;
    border: 1px solid #999;
    span {
      position: absolute;
      left: 0;
      top: 0;
      font-family: var(--sans);
      font-size: 1.2rem;
      padding: 0.5rem 1rem;
      color: #fff;
      background: #999;
    }
    svg {
      width: 100%;
    }
    &.Monogram svg {
      width: 40%;
    }
  }
  @media screen and (max-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
    .LogoItem.Monogram svg {
      width: 70%;
    }
  }
  @media screen and (max-width: 550px) {
    grid-template-rows: 15rem;
    grid-auto-rows: 15rem;
  }
`;

const LogosPage = () => (
  <div>
    <SEO title="Logos" />
    <h2>Logos</h2>
    <br />
    <h3>Overview</h3>
    <p>
      Updater Group consists of three entities:
      <em>Updater, Updater Home Services, and MoveHQ.</em>
    </p>
    <LogoSummary>
      <h3>Updater Group</h3>
      <div className="logoOverview">
        <svg>
          <use xlinkHref={`#${UpdLogo.id}`} />
        </svg>
        <span>+</span>
        <svg>
          <use xlinkHref={`#${UhsLogo.id}`} />
        </svg>
        <span>+</span>
        <svg>
          <use xlinkHref={`#${MhqLogoTagline.id}`} />
        </svg>
      </div>
    </LogoSummary>
    <br />
    <hr />
    <h3>Usage Guidelines</h3>
    <h4>Updater & Updater Home Services</h4>
    <p>
      Always use <strong>Updater</strong> logo. Updater Home Services is
      <em>a part of Updater Inc.</em> and should be communicated as such. Its
      mark is intended for internal communications only and not meant for any
      external materials.
    </p>
    <LogoSet>
      <div className="LogoItem">
        <span>Color Logo</span>
        <svg>
          <use xlinkHref={`#${UpdLogo.id}`} />
        </svg>
      </div>
      <div className="LogoItem" style={{ background: '#ddd' }}>
        <span>B&W Logo</span>
        <svg>
          <use xlinkHref={`#${UpdLogoBk.id}`} />
        </svg>
      </div>
      <div className="LogoItem" style={{ background: '#202020' }}>
        <span>Knock-out Logo</span>
        <svg>
          <use xlinkHref={`#${UpdLogoKo.id}`} />
        </svg>
      </div>
      <div className="LogoItem Monogram">
        <span>Color Monogram</span>
        <svg>
          <use xlinkHref={`#${UpdMonogramInverse.id}`} />
        </svg>
      </div>
      <div className="LogoItem Monogram">
        <span>Inverse Monogram</span>
        <svg>
          <use xlinkHref={`#${UpdMonogram.id}`} />
        </svg>
      </div>
      <div className="LogoItem Monogram" style={{ background: '#202020' }}>
        <span>Knock-out Monogram</span>
        <svg>
          <use xlinkHref={`#${UpdMonogramKo.id}`} />
        </svg>
      </div>
    </LogoSet>
    <br />
    <DownloadButton
      href="https://res.cloudinary.com/updater-marketing/raw/upload/v1585171738/chrysalis-assets/logos/updater-logo-package.zip"
      filetype="Updater Logo Package"
    />
    <br />
    <br />
    <br />
    <br />
    <h4>MoveHQ</h4>
    <p>
      The difference between <em>In-App</em> and <em>Corporate</em> logos is the
      holding company descriptor. For any usage inside MoveHQ apps or/and
      promotional items, use <strong>In-App</strong> version. For any official,
      public documents, and business contexts, use <strong>Corporate</strong>{' '}
      mark. The square <strong>Monogram</strong> is only reserved for
      teaser/thumbnal where the graphic is confined in square shape, and not to
      be used as a full mark.
    </p>
    <LogoSet>
      <div className="LogoItem">
        <span>Color (In-App)</span>
        <svg>
          <use xlinkHref={`#${MhqLogo.id}`} />
        </svg>
      </div>
      <div className="LogoItem" style={{ background: '#202020' }}>
        <span>Knock-out (In-App)</span>
        <svg>
          <use xlinkHref={`#${MhqLogoKo.id}`} />
        </svg>
      </div>
      <div className="LogoItem" style={{ background: '#14424b' }}>
        <span>Knock-out w/ Highlight (In-App)</span>
        <svg>
          <use xlinkHref={`#${MhqLogoKoHighlight.id}`} />
        </svg>
      </div>
      <div className="LogoItem">
        <span>Color (Corporate)</span>
        <svg>
          <use xlinkHref={`#${MhqLogoTagline.id}`} />
        </svg>
      </div>
      <div className="LogoItem" style={{ background: '#202020' }}>
        <span>Knock-out (Corporate)</span>
        <svg>
          <use xlinkHref={`#${MhqLogoTaglineKo.id}`} />
        </svg>
      </div>
      <div className="LogoItem" style={{ background: '#14424b' }}>
        <span>Knock-out w/ Highlight (Corporate)</span>
        <svg>
          <use xlinkHref={`#${MhqLogoTaglineKoHighlight.id}`} />
        </svg>
      </div>
      <div className="LogoItem Monogram">
        <span>Color Monogram</span>
        <svg>
          <use xlinkHref={`#${MhqMonogram.id}`} />
        </svg>
      </div>
      <div className="LogoItem Monogram" style={{ background: '#202020' }}>
        <span>Knock-out Monogram</span>
        <svg>
          <use xlinkHref={`#${MhqMonogramKo.id}`} />
        </svg>
      </div>
    </LogoSet>
    <br />
    <DownloadButton
      href="https://res.cloudinary.com/updater-marketing/raw/upload/v1585173756/chrysalis-assets/logos/movehq-logo-package.zip"
      filetype="MoveHQ Logo Package"
    />
    <br />
    <br />
  </div>
);

export default LogosPage;
