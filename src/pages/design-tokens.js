import React from 'react';
import { Router } from '@reach/router';
import styled from 'styled-components';
import { Link } from 'gatsby';
import Layout from '../components/Layout/Layout';
import SEO from '../components/seo';
import Logos from '../content/Logos';
import Colors from '../content/Colors';
import Icons from '../content/Icons';
import DesignTokensIntro from '../content/design-tokens-intro.mdx';

const Subnav = styled.nav`
  display: inline-flex;
  margin-bottom: 2rem;
  a {
    font-family: var(--sans);
    font-weight: 200;
    font-size: 1.2rem;
    padding: 0 0.5rem;
    text-decoration: none;
    color: var(--upd-blue);
    &:first-of-type {
      padding-left: 0;
    }
    &.active {
      color: var(--upd-pink);
    }
  }
  a + a {
    padding-left: 0;
    &::before {
      content: '•';
      color: #aaa;
      margin-right: 0.5rem;
    }
  }
`;
const Typography = () => <h2>Typography</h2>;
const StyleExample = () => <h2>StyleExample</h2>;
const DesignTokensPage = () => (
  <Layout>
    <h1>Design Tokens</h1>
    {/* <SEO title="Home" /> */}
    <Subnav>
      <Link activeClassName="active" to="/design-tokens/logos">
        Logos
      </Link>
      <Link activeClassName="active" to="/design-tokens/colors">
        Colors
      </Link>
      <Link activeClassName="active" to="/design-tokens/typography">
        Typography
      </Link>
      <Link activeClassName="active" to="/design-tokens/icons">
        Icons
      </Link>
      <Link activeClassName="active" to="/design-tokens/style-examples">
        Examples
      </Link>
    </Subnav>
    <hr />
    <Router>
      <DesignTokensIntro path="/design-tokens" />
      <Logos path="/design-tokens/logos" />
      <Colors path="/design-tokens/colors" />
      <Typography path="/design-tokens/typography" />
      <Icons path="/design-tokens/icons" />
      <StyleExample path="/design-tokens/style-examples" />
    </Router>
  </Layout>
);

export default DesignTokensPage;
