# Chrysalis: Updater Design

All things Updater design: branding, marketing, and product. Currently work in progress, but will eventually expand to include sales materials, print production-ready collaterals, and other internal documents behind authentication layer. Built with `Gatsby`/`React`.

### Immediate Goal

Make the critical brand assets available online, so team members in marketing, sales, or/and support roles can efficiently handle a variety of communications.

### Ultimate Goal

Host all branding, marketing, and product design assets in one place, so that everybody at Updater can easily collaborate and contribute.

### Change Log

- **3/25/2020**: `logos` section ready with download links

- **3/24/2020**: `logos` section draft in place

- **3/23/2020**: `icons` and `colors` sections ready and functional

- **3/20/2020**: Relaunch the site in refactored code

- **Pre-2020**: A working proof of concept for internal review

### Future Plans

- Implement authentication (most likely with `Auth0`)

- Add a variety of other ready-to-use documents including:

  - Sales deck
  - Investor materials
  - Reports and papers (e.g. annual reports, white papers, case studies)
  - Employee resources
